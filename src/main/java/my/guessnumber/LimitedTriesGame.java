/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.guessnumber;

/**
 *
 * @author vhhl
 */

public class LimitedTriesGame implements GuessGame {
    final private GuessGame g; 
    final private int ntries;
    private int tries_counter;
    public LimitedTriesGame(GuessGame g, int ntries) {
        this.g = g;
        this.ntries = ntries;
        this.tries_counter = ntries;
    }
    
    @Override
    public boolean guess(String input, UserInterface ui) {
        boolean correct = this.g.guess(input, ui);
        if(!correct)
            this.tries_counter--;
        if (tries_counter <= 0)
            ui.endGame(this.newGame());
        return correct;
    }
    
    @Override
    public GuessGame newGame() {
        return new LimitedTriesGame(this.g.newGame(), this.ntries);
    }
}