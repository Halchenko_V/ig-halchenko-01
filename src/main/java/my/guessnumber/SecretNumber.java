/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.guessnumber;

/**
 *
 * @author vhhl
 */
public interface SecretNumber {
    int value();
    SecretNumber next();
}