/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.guessnumber;

/**
 *
 * @author vhhl
 */
public class HandleBadInput implements GuessGame {
    final private GuessGame g;
    public HandleBadInput(GuessGame g) {
        this.g = g;
    }
    @Override
    public boolean guess(String input, UserInterface ui) {
        try {
            return this.g.guess(input, ui);
        } catch (NumberFormatException e) {
            ui.badInput();
            return false;
        }
    }
    
    @Override
    public GuessGame newGame() {
        return new HandleBadInput(this.g.newGame());
    }
}