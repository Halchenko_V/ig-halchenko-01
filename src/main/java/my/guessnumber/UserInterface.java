/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.guessnumber;

/**
 *
 * @author vhhl
 */
public interface UserInterface {
    public void badNumber(int difference);
    
    public void correctNumber(GuessGame newGame);
    
    public void badInput();
    
    public void endGame(GuessGame newGame);
}