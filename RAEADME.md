Ten project zawiera grę odgadnięcia liczb od 0 do 100.
Główna klasa uruchamiająca program -- GuessNumber.java.
Należy wpisać liczbę w odpowiednym okienku i wcisnąć OK.

Spełnione wymagania funkcjonalne:
1. Odgadnięcie liczb od 0 do 100.
2. Gra daje 7 sprób na odgadnienie liczby losowej. 
3. Jeśli grać wpisuje złą liczbę, gra podpowiada czy ta liczba jest mniejszą lub większą.
4. Po 7 błędach gra się skończy.
5. Przycisk "Start over" służy do tego żeby zacznąć nową grę.

Spełnione wymagania niefunkcjonalne:
1. Architektura jest zaprojektowana w taki sposób by oddzielić interfejs użyktownika od logiki programu.
