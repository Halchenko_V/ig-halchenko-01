/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.guessnumber;

import java.util.Random; 

/**
 *
 * @author vhhl
 */
public class RandomNumber implements SecretNumber {
    private final Random rand;
    private int number = -1;
    RandomNumber(long seed) {
        this(new Random(seed));
    }
    
    RandomNumber(Random r) {
        this.rand = r;
    }
    
    public int value(){
       if (this.number == -1) {
           this.number = this.rand.nextInt(101);
       }
       System.out.println("The number: " + this.number);
       return this.number;
    }
    
    public SecretNumber next() {
        return new RandomNumber(this.rand);
    }
}
