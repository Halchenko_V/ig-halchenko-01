/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.guessnumber;

/**
 *
 * @author vhhl
 */
public class SimpleGuessGame implements GuessGame {
    final private SecretNumber secretNumber;
    
    public SimpleGuessGame(SecretNumber secNum) {
        this.secretNumber = secNum;
    }
    
    @Override
    public boolean guess(String input, UserInterface ui) {
        int value;
        try {
            value = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            ui.badInput();
            return false;
        }
        if (value != this.secretNumber.value()) {
            ui.badNumber(this.secretNumber.value() - value);
            return false;
        }
        ui.correctNumber(this.newGame());
        return true;
    }

    @Override
    public GuessGame newGame() {
        return new SimpleGuessGame(this.secretNumber.next());
    }
}

